import Vue from 'vue'
import Router from 'vue-router'
import MainLeftTemplate from './components/MainLeftTemplate.vue'
import DetailArticle from './components/DetailArticle.vue'
import RegisterUser from './components/RegisterUser.vue'
import RegisterUserProfile from './components/RegisterUserProfile.vue'
import Login from './components/Login.vue'
import PostArticle from './components/PostArticle.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'mainleft',
            component: MainLeftTemplate,           
        },
        {
            path: '/detailarticle/:idArticle',
            name: 'detailarticle',
            component: DetailArticle,
            props: true
           
        },
        {
            path: '/registeruser',
            name: 'registeruser',
            component: RegisterUser,
        
        },
        {
            path: '/registeruserprofile/:id',
            name: 'registeruserprofile',
            component: RegisterUserProfile,
            props: true
        
        },
        {
            path: '/login',
            name: 'login',
            component: Login,

        },
        {
            path: '/postarticle',
            name: 'postarticle',
            component: PostArticle,
            meta: { requiresAuth: true },
        }
        
    ],
  

    
    
})
