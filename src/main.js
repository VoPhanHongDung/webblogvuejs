import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import router from './router'
import VeeValidate from 'vee-validate'
import store from './store'
import VueFroala from 'vue-froala-wysiwyg'
// Require Froala Editor js file.
require('froala-editor/js/froala_editor.pkgd.min')

// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('froala-editor/css/froala_style.min.css')
// Jquery library
import * as $ from 'jquery'
window['$'] = $;
window['jQuery'] = $;

Vue.use(VueFroala)
Vue.config.productionTip = false
Vue.use(BootstrapVue);
Vue.use(VeeValidate);

  router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth) && !store.state.username) {
            next({ path: '/login', query: { redirect: to.fullPath } });
        } else {
            next();
        }
    })

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
