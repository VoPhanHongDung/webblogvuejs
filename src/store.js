import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        username : localStorage.getItem("username"),    
    },
    mutations: {
        ISLOGIN(state ,username){
            state.username = username
        },
        ISLOGOUT(state) {
            state.username = null
        }
    },
    actions: {
        isLogin({commit}, username) {
            commit('ISLOGIN',username)
        },
        isLogout({ commit }) {
            commit('ISLOGOUT')
        },
    }
})
